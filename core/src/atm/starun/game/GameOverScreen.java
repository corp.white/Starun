package atm.starun.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import atm.starun.game.helpers.AssetLoader;

public class GameOverScreen implements Screen {
	private Viewport viewport;
	private Stage stage;
	private Button playAgainButton, mainMenuButton;

	final Starun game;

	public GameOverScreen(final Starun game) {
		this.game = game;
		viewport = new FitViewport(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT, new OrthographicCamera());
		stage = new Stage(viewport, game.batch);

		// Create buttons with their labels.
		playAgainButton = new Button(AssetLoader.starunskin);
		Label playAgainLabel = new Label("Retry", AssetLoader.starunskin);
		playAgainButton.add(playAgainLabel);

		mainMenuButton = new Button(AssetLoader.starunskin);
		Label mainMenuLabel = new Label("Main\nmenu", AssetLoader.starunskin);
		mainMenuButton.add(mainMenuLabel);

		// On-click actions

		mainMenuButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.menuMusic.play();
				AssetLoader.mainMenuSound.play();
				game.setScreen(new MainMenuScreen(game));
				dispose();
			}
		});

		playAgainButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.menuMusic.stop();
				AssetLoader.gameMusic.play();
				AssetLoader.playAgainSound.play();
				game.setScreen(new GameScreen(game));
				dispose();
			}
		});

		Table table = new Table();
		table.center();
		table.setFillParent(true);

		Label gameOverLabel = new Label("GAME OVER", AssetLoader.titleFontStyle);
		Label scoreLabel = new Label("Score: " + GameScreen.score, AssetLoader.textFontStyle);
		Label highScoreLabel = new Label("High score: " + Starun.getHighScore(), AssetLoader.textFontStyle);
		Label newRecordLabel = new Label("New record!", AssetLoader.recordFontStyle);

		table.add(gameOverLabel).expandX();
		table.row();
		table.add(playAgainButton).padTop(30f);
		table.row();
		table.add(mainMenuButton).padTop(20f);
		table.row();
		table.add(scoreLabel).padTop(30f);
		table.row();
		table.add(highScoreLabel);

		if (GameScreen.score == Starun.getHighScore() && Starun.getHighScore() != 0
				&& Starun.getHighScore() > Starun.previousHighScore) {
			table.row();
			table.add(newRecordLabel).padTop(35f);
			AssetLoader.newRecordSound.play();
		}
		stage.addActor(table);
		Gdx.input.setInputProcessor(stage);

	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		game.batch.begin();
		Starun.drawBackground(game.batch, 5);
		game.batch.end();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
