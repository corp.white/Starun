package atm.starun.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import atm.starun.game.helpers.AssetLoader;

public class CreditsScreen implements Screen {

	final Starun game;
	private OrthographicCamera camera;
	private Viewport viewport;
	private Stage stage;
	private Button backToMenuButton;

	public CreditsScreen(final Starun game) {
		this.game = game;

		camera = new OrthographicCamera();
		viewport = new FitViewport(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT, camera);
		stage = new Stage(viewport, game.batch);

		// Create back to menu button with its label.
		backToMenuButton = new Button(AssetLoader.starunskin);
		Label backLabel = new Label("Main\nmenu", AssetLoader.starunskin);
		backToMenuButton.add(backLabel);

		// Set its on-click action.
		backToMenuButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.mainMenuSound.play();
				game.setScreen(new MainMenuScreen(game));
				dispose();
			}
		});

		Table table = new Table();
		table.center();
		table.setFillParent(true);

		Label creditsLabel = new Label("Developed by Antonio Torres Moríñigo."
				+ "\nDesign by Paula Alba & Jaime Torres." + "\nLicensed under GPLv3."
				+ "\nPowered by libGDX (Apache 2.0)." + "\n\nAssets:" + "\nSound effects by Lokif under CC-0."
				+ "\nButtons by @CamTatz under CC-0." + "\nGame music by Gichco under CC-0."
				+ "\nMenu music by bart under GPLv3." + 
				"\nFont by J. Enaguas under CC-0." +
				"\nOpenGameArt.org", AssetLoader.creditFontStyle);
		Label thankYouLabel = new Label(" Thank you\nfor playing!", AssetLoader.recordFontStyle);
		creditsLabel.setAlignment(0);
		table.add(creditsLabel);
		table.row();
		table.add(thankYouLabel).padTop(20);
		table.row();
		table.add(backToMenuButton).padTop(30f);

		stage.addActor(table);
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		game.batch.begin();
		Starun.drawBackground(game.batch, 5);
		game.batch.end();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		stage.dispose();
	}

}
