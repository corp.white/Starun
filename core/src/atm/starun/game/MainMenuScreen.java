package atm.starun.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import atm.starun.game.helpers.AssetLoader;

public class MainMenuScreen implements Screen {

	final Starun game;
	OrthographicCamera camera;
	Viewport viewport;
	Stage stage;
	Button playButton, creditsButton, donateButton;

	public MainMenuScreen(final Starun game) {
		this.game = game;

		camera = new OrthographicCamera();
		viewport = new FitViewport(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT, camera);
		stage = new Stage(viewport, game.batch);

		// Create the play button with its respective label.
		playButton = new Button(AssetLoader.starunskin);
		Label testLabel = new Label("Play!", AssetLoader.starunskin);
		playButton.add(testLabel);

		// Set its on-click action.
		playButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.playAgainSound.play();
				AssetLoader.menuMusic.stop();
				AssetLoader.gameMusic.play();
				game.setScreen(new GameScreen(game));
				dispose();
			}
		});

		// Create the credits button with its respective label.
		creditsButton = new Button(AssetLoader.starunskin);
		Label creditsLabel = new Label("Credits", AssetLoader.starunskin);
		creditsButton.add(creditsLabel);

		// Set its on-click action.
		creditsButton.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.mainMenuSound.play();
				game.setScreen(new CreditsScreen(game));
				dispose();
			}
		});
		
		// Create Donate button with its respective label.
		donateButton = new Button(AssetLoader.starunskin, "donate");
		Label donateLabel = new Label("Donate", AssetLoader.starunskin);
		donateLabel.setFontScale(0.75f);
		donateButton.add(donateLabel);
		
		//Set its on-click action.
		donateButton.addListener(new ChangeListener() {
			
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				AssetLoader.mainMenuSound.play();
				// This will open system's default Internet browser and open the paypal donations section.
				Gdx.net.openURI("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M7YKJ3W9E9BVY");
			}
		});
		
		Table table = new Table();
		table.setFillParent(true);

		Label titleLabel = new Label("Starun", AssetLoader.titleFontStyle);
		Label highScoreLabel = new Label("High score: " + Starun.getHighScore(), AssetLoader.textFontStyle);
		table.row();
		table.add(titleLabel);
		table.row();
		table.add(playButton).padTop(60);
		table.row();
		table.add(creditsButton).padTop(10f);
		table.row();
		table.add(donateButton).padTop(20).size(163, 70);
		table.row();
		table.add(highScoreLabel).padTop(30f);
		stage.addActor(table);

		// Start the music
		AssetLoader.menuMusic.play();

		// Set input processor
		Gdx.input.setInputProcessor(stage);

	}

	@Override
	public void show() {
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		game.batch.begin();
		Starun.drawBackground(game.batch, 5);
		game.batch.end();
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void hide() {
	}

	@Override
	public void dispose() {
		stage.dispose();

	}

}
