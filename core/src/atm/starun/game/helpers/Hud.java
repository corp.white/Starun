package atm.starun.game.helpers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import atm.starun.game.GameScreen;
import atm.starun.game.Starun;

public class Hud implements Disposable {
	public Stage stage;
	private Viewport viewport;
	
	private Integer score;
	private Integer collisions;
	
	//Scene2D widgets
	private Label scoreLabel;
	private Label lifeLabel;
	
	public Hud(Batch batch) {
		//define variables
		score = GameScreen.score;
		collisions = GameScreen.collisions;
		
		//setup viewport and stage
		viewport = new FitViewport(Starun.SCREEN_WIDTH, Starun.SCREEN_HEIGHT, new OrthographicCamera());
		stage = new Stage(viewport, batch);
		
		//define Table
		Table table = new Table();
		table.top();
		table.setFillParent(true);
		
		//define labels
		scoreLabel = new Label(score.toString(), AssetLoader.titleFontStyle);
		lifeLabel = new Label((3 - collisions) + "/3", AssetLoader.titleFontStyle);
		
		//add labels to table
		table.add(scoreLabel).expandX().align(Align.left).padLeft(5);
		table.add(lifeLabel).expandX().align(Align.right).padRight(5);
		
		stage.addActor(table);
		
	}

	public void update(float delta) {
		scoreLabel.setText(GameScreen.score.toString());
		lifeLabel.setText((3 - GameScreen.collisions) + "/3");
	}
	
	@Override
	public void dispose() {
		stage.dispose();
	}
}
