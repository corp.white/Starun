package atm.starun.game.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import atm.starun.game.Starun;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Starun";
		config.width = 360;
		config.height = 640;
		config.resizable = false;
		new LwjglApplication(new Starun(), config);
	}
}
